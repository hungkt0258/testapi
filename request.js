{
  nam: 2021, // năm request
  don_vi: 'HCMUIT', // đơn vị trực thuộc ĐHQG TPHCM
  ho_so: [
    {
      id: 1,
      ten_van_ban: "hồ sơ mở ngành",
      trang_thai: "Đã xét duyệt",
      ten_nguoi_nop_ho_so: "Ths.Nguyễn Văn A",
      file_dinh_kem: "(link tải file)",
    },
    {
      id: 2,
      ten_van_ban: "chương trình đào tạo mới",
      trang_thai: "Đang xử lý",
      ten_nguoi_nop_ho_so: "Ths.Nguyễn Văn B",
      file_dinh_kem: "(link tải file)",
    },
    {
      id: 3,
      ten_van_ban: "xin mở lớp Giải Tích 1",
      trang_thai: "Không xét duyệt",
      ten_nguoi_nop_ho_so: "Ths.Nguyễn Văn C",
      file_dinh_kem: "(link tải file)",
    },
    {
      id: 4,
      ten_van_ban: "xin mở lớp Đường Lối Cách Mạng Đảng Cộng Sản",
      trang_thai: "Chưa xử lý",
      ten_nguoi_nop_ho_so: "Ths.Nguyễn Văn D",
      file_dinh_kem: "(link tải file)",
    }
  ]
}
