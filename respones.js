{
  nam: 2021, // năm request
  don_vi: 'HCMUIT', // đơn vị trực thuộc ĐHQG TPHCM
  khoa_hoc: 16,
  data: [
    {
      id: 1,
      ma_lop: "CQ.16.CNTT", // theo nhà trường quy định
      ten_lop: "Công nghệ thông tin k16",
      nien_khoa: "2016-2021",
      he_dao_tao: "Chính quy",
      loai_hinh_dao_tao: "Tín chỉ",
      du_lieu_sinh_vien: [
        {
          id:1,
          ma_sv: "1601000", //mã này do nhà trường quy định
          ten_sv: "Lê Văn A",
          ngay_sinh: "19980101", // nếu có thể thì trả về 01/01/1998
          noi_sinh: "Bình Đình",
          mon_hoc: [
            {
              ma_mon_hoc: "GQP201.3",
              ten_mon_hoc: "Giáo dục QP-AN F1",
              so_tc: 3, // số tín chỉ
              hoc_ky: 1,
              nam_hoc: 2016-2017,
              diem_thi: {
                lan_1: 6.9,
                thi_lai: "",
              }
            },
            {
              ma_mon_hoc: "GQP202.2",
              ten_mon_hoc: "Giáo dục QP-AN F2",
              so_tc: 2, // số tín chỉ
              hoc_ky: 1,
              nam_hoc: 2016-2017,
              diem_thi: {
                lan_1: 3.9,
                thi_lai: 3.9,
              }
            }
          ]
        },
        {
          id:2,
          ma_sv: "1601001", //mã này do nhà trường quy định
          ten_sv: "Lê Văn B",
          ngay_sinh: "19980102", // nếu có thể thì trả về 02/01/1998
          noi_sinh: "Bình Đình",
          mon_hoc: [
            {
              ma_mon_hoc: "GQP201.3",
              ten_mon_hoc: "Giáo dục QP-AN F1",
              so_tc: 3, // số tín chỉ
              hoc_ky: 1,
              nam_hoc: 2016-2017,
              diem_thi: {
                lan_1: 3.9,
                thi_lai: "",
              }
            },
            {
              ma_mon_hoc: "GQP202.2",
              ten_mon_hoc: "Giáo dục QP-AN F2",
              so_tc: 2, // số tín chỉ
              hoc_ky: 1,
              nam_hoc: 2016-2017,
              diem_thi: {
                lan_1: 5.9,
                thi_lai: 7.9,
              }
            }
          ]
        },
      ]
    },
    {
      id: 2,
      ma_lop: "KCQ.16.GTCC", // theo nhà trường quy định
      ten_lop: "Công trình giao thông công chính k16",
      nien_khoa: "2016-2021",
      he_dao_tao: "Không chính quy",
      loai_hinh_dao_tao: "Tín chỉ",
      du_lieu_sinh_vien: [
        {
          id: 1,
          ma_sv: "1601000", //mã này do nhà trường quy định
          ten_sv: "Lê Văn A",
          ngay_sinh: "19980101", // nếu có thể thì trả về 01/01/1998
          noi_sinh: "Bình Đình",
          mon_hoc: [
            {
              ma_mon_hoc: "GQP201.3",
              ten_mon_hoc: "Giáo dục QP-AN F1",
              so_tc: 3, // số tín chỉ
              hoc_ky: 1,
              nam_hoc: 2016-2017,
              diem_thi: {
                lan_1: 6.9,
                thi_lai: "",
              }
            },
            {
              ma_mon_hoc: "GQP202.2",
              ten_mon_hoc: "Giáo dục QP-AN F2",
              so_tc: 2, // số tín chỉ
              hoc_ky: 1,
              nam_hoc: 2016-2017,
              diem_thi: {
                lan_1: 3.9,
                thi_lai: 3.9,
              }
            }
          ]
        },
        {
          id:2,
          ma_sv: "1601001", //mã này do nhà trường quy định
          ten_sv: "Lê Văn B",
          ngay_sinh: "19980102", // nếu có thể thì trả về 02/01/1998
          noi_sinh: "Bình Đình",
          mon_hoc: [
            {
              ma_mon_hoc: "GQP201.3",
              ten_mon_hoc: "Giáo dục QP-AN F1",
              so_tc: 3, // số tín chỉ
              hoc_ky: 1,
              nam_hoc: 2016-2017,
              diem_thi: {
                lan_1: 3.9,
                thi_lai: "",
              }
            },
            {
              ma_mon_hoc: "GQP202.2",
              ten_mon_hoc: "Giáo dục QP-AN F2",
              so_tc: 2, // số tín chỉ
              hoc_ky: 1,
              nam_hoc: 2016-2017,
              diem_thi: {
                lan_1: 5.9,
                thi_lai: 7.9,
              }
            }
          ]
        },
      ]
    },

  ]
}
